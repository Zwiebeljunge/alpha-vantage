{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module ForeignExchange
  ( ExchangeRates (..),
    exchangeRates,
    exchangeRatesAsAlphaRequest,
  )
where

import Api
import Polysemy
import Types

data ExchangeRates r a where
  ExchangeRates :: FromCurrency -> ToCurrency -> ExchangeRates r (Either String CurrencyExchangeRate)

makeSem ''ExchangeRates

exchangeRatesAsAlphaRequest :: Sem (ExchangeRates : r) a -> Sem (AlphaRequest : r) a
exchangeRatesAsAlphaRequest =
  rewrite $ \case
    ExchangeRates from to -> AlphaRequest CURRENCY_EXCHANGE_RATE (from <&&> to)
