{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Api
  ( AlphaRequest (..),
    alphaRequest,
    runAlphaRequest,
  )
where

import Control.Applicative
import Control.Monad
import Data.Aeson hiding ((<?>))
import Network.HTTP.Simple
import Polysemy
import Types

data AlphaRequest r a where
  AlphaRequest :: (FromJSON a, Function f) => f -> [Maybe Param] -> AlphaRequest r (Either String a)

makeSem ''AlphaRequest

newtype AlphaResult a = AlphaResult {unAlphaResult :: Either String a}

instance FromJSON a => FromJSON (AlphaResult a) where
  parseJSON v = AlphaResult <$> ((Right <$> parseJSON v) <|> pure (Left $ show v))

runAlphaRequest :: Members '[Embed IO] r => ApiKey -> Sem (AlphaRequest : r) (Either String a) -> Sem r (Either String a)
runAlphaRequest apiKey = interpret $ \case
  AlphaRequest fun params -> (unAlphaResult <=< eitherDecode . getResponseBody) <$> httpLbs (toRequest $ apiBase <?> (functionParam fun : formatParam apiKey : params))

apiBase :: URLBase
apiBase = "https://www.alphavantage.co/query"
