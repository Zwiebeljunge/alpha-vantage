{-# LANGUAGE DataKinds #-}

module StockSeries
  ( intraday,
    daily,
    dailyAdjusted,
    weekly,
    weeklyAdjusted,
    monthly,
    monthlyAdjusted,
    quote,
  )
where

import Api
import Control.Monad.IO.Class
import Data.Time
import Polysemy
import Types

intraday :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
intraday sym int out = alphaRequest TIME_SERIES_INTRADAY (sym <&> int <&&> out)

daily :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries Day))
daily sym int out = alphaRequest TIME_SERIES_DAILY (sym <&> int <&&> out)

dailyAdjusted :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
dailyAdjusted sym int out = alphaRequest TIME_SERIES_DAILY_ADJUSTED (sym <&> int <&&> out)

weekly :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
weekly sym int out = alphaRequest TIME_SERIES_WEEKLY (sym <&> int <&&> out)

weeklyAdjusted :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
weeklyAdjusted = undefined

monthly :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
monthly sym int out = alphaRequest TIME_SERIES_MONTHLY (sym <&> int <&&> out)

monthlyAdjusted :: Members '[AlphaRequest] r => Symbol -> StockInterval -> Outputsize -> Sem r (Either String (Types.TimeSeries UTCTime))
monthlyAdjusted = undefined

quote :: Members '[AlphaRequest] r => Symbol -> Sem r (Either String Quote)
quote sym = alphaRequest GLOBAL_QUOTE [formatParam sym]
