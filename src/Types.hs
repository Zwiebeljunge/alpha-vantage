{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UndecidableInstances #-}

module Types
  ( toRequest,
    (<&>),
    (<&&>),
    (<?>),
    ToParam (..),
    Param (..),
    URL,
    URLBase,
    ApiKey (..),
    Function (..),
    StockFunction (..),
    functionParam,
    Symbol (..),
    StockInterval (..),
    Outputsize (..),
    Datatype (..),
    TMEntry (..),
    TimeSeries (..),
    Quote (..),
    ForexFunction (..),
    Currency (..),
    gold,
    euro,
    usd,
    FromCurrency (..),
    ToCurrency (..),
    CurrencyExchangeRate (..),
  )
where

import Control.Applicative
import Data.Aeson hiding ((<?>))
import Data.Aeson.Types hiding ((<?>))
import qualified Data.HashMap.Strict as HM
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.String
import Data.Time
import Debug.Trace
import Network.HTTP.Simple

data URL = URL URLBase [Maybe Param]

toRequest :: URL -> Request
toRequest = fromString . ("GET " <>) . formatURL

formatURL :: URL -> String
formatURL (URL (URLBase base) params) = base <> "?" <> intercalate "&" (mapMaybe (fmap formatQueryParam) params)
  where
    formatQueryParam (Param name val) = name <> "=" <> val

newtype URLBase = URLBase {unBase :: String}

data Param = Param
  { paramName :: String,
    paramValue :: String
  }

instance IsString URLBase where
  fromString = URLBase

class ToParam p where
  formatParam :: p -> Maybe Param

instance ToParam ApiKey where
  formatParam = Just . Param "apikey" . apiKey

instance ToParam Symbol where
  formatParam = Just . Param "symbol" . unSymbol

instance ToParam StockInterval where
  formatParam = Just . Param "interval" . formatInterval

instance ToParam Outputsize where
  formatParam = Just . Param "outputsize" . formatOutputsize

instance ToParam p => ToParam (Maybe p) where
  formatParam p = p >>= formatParam

infixr 5 <?>

(<?>) :: URLBase -> [Maybe Param] -> URL
(<?>) = URL

(</>) :: URLBase -> URLBase -> URLBase
(</>) = undefined

infixr 5 <&>

(<&>) :: ToParam p => p -> [Maybe Param] -> [Maybe Param]
p <&> ps = formatParam p : ps

infixr 5 <&&>

(<&&>) :: (ToParam p1, ToParam p2) => p1 -> p2 -> [Maybe Param]
p1 <&&> p2 = [formatParam p1, formatParam p2]

newtype ApiKey = ApiKey {apiKey :: String}

instance IsString ApiKey where
  fromString = ApiKey

data StockFunction
  = TIME_SERIES_INTRADAY
  | TIME_SERIES_DAILY
  | TIME_SERIES_WEEKLY
  | TIME_SERIES_MONTHLY
  | TIME_SERIES_DAILY_ADJUSTED
  | TIME_SERIES_WEEKLY_ADJUSTED
  | TIME_SERIES_MONTHLY_ADJUSTED
  | GLOBAL_QUOTE
  deriving (Show)

class Function f where
  toFunctionName :: f -> String

functionParam :: Function f => f -> Maybe Param
functionParam = Just . Param "function" . toFunctionName

instance Function StockFunction where
  toFunctionName = show

newtype Symbol = Symbol {unSymbol :: String}
  deriving (Show)

instance IsString Symbol where
  fromString = Symbol

data StockInterval = SMin1 | SMin5 | SMin15 | SMin30 | SMin60
  deriving (Show)

formatInterval :: StockInterval -> String
formatInterval = \case
  SMin1 -> "1min"
  SMin5 -> "5min"
  SMin15 -> "15min"
  SMin30 -> "30min"
  SMin60 -> "60min"

instance FromJSON StockInterval where
  parseJSON (String "1min") = pure SMin1
  parseJSON (String "5min") = pure SMin5
  parseJSON (String "15min") = pure SMin15
  parseJSON (String "30min") = pure SMin30
  parseJSON (String "60min") = pure SMin60

data Outputsize = Compact | Full
  deriving (Show)

formatOutputsize :: Outputsize -> String
formatOutputsize = \case
  Compact -> "compact"
  Full -> "full"

instance FromJSON Outputsize where
  parseJSON (String "Compact") = pure Compact
  parseJSON (String "Full") = pure Full

data Datatype = Json | Csv

data TMEntry = TMEntry
  { open :: Double,
    high :: Double,
    low :: Double,
    close :: Double,
    volume :: Int
  }
  deriving (Show)

instance FromJSON TMEntry where
  parseJSON = withObject "TMEntry" $ \o ->
    TMEntry <$> (read <$> o .: "1. open")
      <*> (read <$> o .: "2. high")
      <*> (read <$> o .: "3. low")
      <*> (read <$> o .: "4. close")
      <*> (read <$> o .: "5. volume")

data TimeSeries t = TimeSeries
  { symbol :: Symbol,
    lastRefreshed :: t,
    series :: Map t TMEntry
  }
  deriving (Show)

instance (Ord t, ParseTime t) => FromJSON (TimeSeries t) where
  parseJSON v =
    withObject
      "TimeSeries"
      ( \o -> do
          traceShowM o
          TimeSeries <$> ((o .: "Meta Data") >>= \x -> Symbol <$> x .: "2. Symbol")
            <*> ((o .: "Meta Data") >>= (.: "3. Last Refreshed") >>= \tStr -> parseTimeM True defaultTimeLocale "%Y-%m-%d %H:%M:%S" tStr <|> parseTimeM True defaultTimeLocale "%Y-%m-%d" tStr)
            <*> (parseJSON v >>= parseTMEntries . (snd :: (String, a) -> a) . head . filter (isInfixOf "Time Series" . fst) . HM.toList)
      )
      v
    where
      parseTMEntries :: (Ord t, ParseTime t) => Value -> Parser (Map t TMEntry)
      parseTMEntries x = parseJSON x >>= fmap M.fromList . mapM parseTMEntry . HM.toList

      parseTMEntry :: ParseTime t => (String, TMEntry) -> Parser (t, TMEntry)
      parseTMEntry (time, entry) =
        (,)
          <$> ( parseTimeM True defaultTimeLocale "%Y-%m-%d %H:%M:%S" time
                  <|> parseTimeM True defaultTimeLocale "%Y-%m-%d" time
              )
          <*> pure entry

data Quote = Quote
  { qSymbol :: Symbol,
    qOpen :: Double,
    qHigh :: Double,
    qLow :: Double,
    qPrice :: Double,
    qVolume :: Int,
    qLatestTradingDay :: Day,
    qPreviousClose :: Double,
    qChange :: Double,
    qChangePercent :: Double
  }
  deriving (Show)

instance FromJSON Quote where
  parseJSON = withObject "Quote" $ \x ->
    (x .: "Global Quote")
      >>= withObject
        "Global Quote"
        ( \o ->
            Quote <$> (Symbol <$> o .: "01. symbol")
              <*> (read <$> o .: "02. open")
              <*> (read <$> o .: "03. high")
              <*> (read <$> o .: "04. low")
              <*> (read <$> o .: "05. price")
              <*> (read <$> o .: "06. volume")
              <*> (read <$> o .: "07. latest trading day")
              <*> (read <$> o .: "08. previous close")
              <*> (read <$> o .: "09. change")
              <*> (read . init <$> o .: "10. change percent")
        )

data ForexFunction = CURRENCY_EXCHANGE_RATE
  deriving (Show)

instance Function ForexFunction where
  toFunctionName = show

newtype Currency = Currency {unCurrency :: String}
  deriving (Show, Read)

gold :: Currency
gold = Currency "XAU"

euro :: Currency
euro = Currency "EUR"

usd :: Currency
usd = Currency "USD"

instance FromJSON Currency where
  parseJSON v = Currency <$> parseJSON v

newtype FromCurrency = FromCurrency Currency

newtype ToCurrency = ToCurrency Currency

instance ToParam FromCurrency where
  formatParam (FromCurrency (Currency c)) = Just $ Param "from_currency" c

instance ToParam ToCurrency where
  formatParam (ToCurrency (Currency c)) = Just $ Param "to_currency" c

data CurrencyExchangeRate = CurrencyExchangeRate
  { from :: Currency,
    fromName :: Maybe String,
    to :: Currency,
    toName :: Maybe String,
    exchangeRate :: Double,
    exLastRefreshed :: UTCTime,
    timeZone :: String,
    bidPrice :: Double,
    askPrice :: Double
  }
  deriving (Show, Read)

instance FromJSON CurrencyExchangeRate where
  parseJSON = withObject "CurrencyExchangeRate" $ \o ->
    o .: "Realtime Currency Exchange Rate"
      >>= withObject
        "CurrencyExchangeRate Inner"
        ( \x ->
            CurrencyExchangeRate <$> x .: "1. From_Currency Code"
              <*> x .: "2. From_Currency Name"
              <*> x .: "3. To_Currency Code"
              <*> x .: "4. To_Currency Name"
              <*> (read <$> x .: "5. Exchange Rate")
              <*> (x .: "6. Last Refreshed" >>= parseTimeM True defaultTimeLocale "%F %T")
              <*> x .: "7. Time Zone"
              <*> (read <$> x .: "8. Bid Price")
              <*> (read <$> x .: "9. Ask Price")
        )
