{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module TechnicalIndicators
  ( IndicatorInterval (..),
    SeriesType (..),
    TimePeriod (..),
    sma,
    ema,
  )
where

import Api
import Control.Applicative
import Control.Monad.IO.Class
import Data.Aeson
import Data.Aeson.Types
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Time
import Polysemy
import Types

data IndicatorInterval t where
  IMin1 :: IndicatorInterval UTCTime
  IMin5 :: IndicatorInterval UTCTime
  IMin15 :: IndicatorInterval UTCTime
  IMin30 :: IndicatorInterval UTCTime
  IMin60 :: IndicatorInterval UTCTime
  IDaily :: IndicatorInterval Day
  IWeekly :: IndicatorInterval Day
  IMonthly :: IndicatorInterval Day

instance ToParam (IndicatorInterval t) where
  formatParam = Just . Param "interval" . formatIndicatorInterval
    where
      formatIndicatorInterval :: IndicatorInterval t -> String
      formatIndicatorInterval = \case
        IMin1 -> "1min"
        IMin5 -> "5min"
        IMin15 -> "15min"
        IMin30 -> "30min"
        IMin60 -> "60min"
        IDaily -> "daily"
        IWeekly -> "weekly"
        IMonthly -> "monthly"

newtype TimePeriod = TimePeriod {unTimePeriod :: Int}

instance ToParam TimePeriod where
  formatParam = Just . Param "time_period" . show . unTimePeriod

data SeriesType = Close | Open | High | Low

instance ToParam SeriesType where
  formatParam = Just . Param "series_type" . formatSeriesType
    where
      formatSeriesType = \case
        Close -> "close"
        Open -> "open"
        High -> "high"
        Low -> "low"

newtype TimeSeries t a = TimeSeries {unTimeSeries :: Map t a}
  deriving (Show)

instance (FromJSON a, Ord t, ParseTime t) => FromJSON (TechnicalIndicators.TimeSeries t a) where
  parseJSON x = parseJSON x >>= parseTimeSeries . snd . M.findMin . M.filterWithKey (\k _ -> isPrefixOf "Technical Analysis" k)
    where
      parseTimeSeries :: (FromJSON a, Ord t, ParseTime t) => Map String Value -> Parser (TechnicalIndicators.TimeSeries t a)
      parseTimeSeries = fmap (TechnicalIndicators.TimeSeries . M.fromList) . mapM parseSeriesEntry . M.toList

      parseSeriesEntry (t, v) = do
        time <- parseTimeM True defaultTimeLocale "%Y-%m-%d %H:%M" t <|> parseTimeM True defaultTimeLocale "%Y-%m-%d" t
        value <- parseJSON v
        pure (time, value)

data IndicatorFunction = SMA | EMA
  deriving (Show)

instance Function IndicatorFunction where
  toFunctionName = show

indicatorRequest :: (FromJSON a, Ord t, ParseTime t, Members '[AlphaRequest] r) => IndicatorFunction -> Symbol -> IndicatorInterval t -> TimePeriod -> SeriesType -> Sem r (Either String (TechnicalIndicators.TimeSeries t a))
indicatorRequest fun sym int per series = alphaRequest fun (sym <&> int <&> per <&&> series)

newtype SMAVal = SMAVal {unSMA :: Double}
  deriving (Show)

instance FromJSON SMAVal where
  parseJSON = withObject "SMAVal" $ \o ->
    SMAVal <$> (read <$> o .: "SMA")

sma :: (Ord t, ParseTime t, Members '[AlphaRequest] r) => Symbol -> IndicatorInterval t -> TimePeriod -> SeriesType -> Sem r (Either String (TechnicalIndicators.TimeSeries t SMAVal))
sma = indicatorRequest SMA

newtype EMAVal = EMAVal {unEMA :: Double}
  deriving (Show)

instance FromJSON EMAVal where
  parseJSON = withObject "EMAVal" $ \o ->
    EMAVal <$> (read <$> o .: "EMA")

ema :: (Ord t, ParseTime t, Members '[AlphaRequest] r) => Symbol -> IndicatorInterval t -> TimePeriod -> SeriesType -> Sem r (Either String (TechnicalIndicators.TimeSeries t EMAVal))
ema = indicatorRequest EMA
