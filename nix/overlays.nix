let
  customHaskellPackages = self: super: {
    haskellPackages = super.haskellPackages.override {
      overrides = hself: hsuper:
        let
          dontCheck = super.haskell.lib.dontCheck;
          dontHaddock = super.haskell.lib.dontHaddock;

          # Disable Haddock generation and profiling by default. The former
          # can be done via cabal, while the latter should be enabled on
          # demand.
          defaultMod = drv:
            super.haskell.lib.dontHaddock
            (super.haskell.lib.disableLibraryProfiling drv);

          # A recent version of polysemy.
          polysemySrc = super.fetchFromGitHub {
            owner = "polysemy-research";
            repo = "polysemy";
            rev = "21fcd8a8492b45d7b5c3fee6adb44050118d8d17";
            sha256 = "0zx08z9lhvxjhqnygryh28pskhjryrmm1is8rhfizsfzffdgs77w";
          };

          # Use the newest version of polysemy to circumvent dependency
          # version problems.
          polysemy =
            defaultMod (hself.callCabal2nix "polysemy" polysemySrc { });

          polysemy-plugin = defaultMod
            (hself.callCabal2nix "polysemy" (polysemySrc + /polysemy-plugin)
              { });

          alpha-vantage-src = self.nix-gitignore.gitignoreSource [
            "*.git"
            "dist"
            "dist-newstyle"
          ] ../.;
          alpha-vantage =
            hself.callCabal2nix "alpha-vantage" alpha-vantage-src { };
        in {
          # We add ourselves to the set of haskellPackages.
          inherit alpha-vantage;

          inherit polysemy;
          inherit polysemy-plugin;
        };
    };
  };
in [ customHaskellPackages ]
